# Проект создан для тестирования видов различных BOM и утилит для них

## SBOM

В проекте представлено две утилиты для создания SBOM: https://github.com/anchore/syft и https://github.com/CycloneDX/cyclonedx-gomod.

## IBOM

Представлена утилита https://github.com/bridgecrewio/checkov, просматривающая файл kubernetes-manifest.yml

## KBOM

Утилита https://github.com/ksoclabs/kbom/